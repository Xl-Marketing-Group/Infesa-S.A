<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="assets/img/fav-icon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>INFENSA S.A</title>

        <!-- Icon css link -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/vendors/elegant-icon/style.css" rel="stylesheet">
        <link href="assets/vendors/themify-icon/themify-icons.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="assets/vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="assets/vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="assets/vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="assets/vendors/animate-css/animate.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="assets/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">

        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>


        <!--================Search Area =================-->
        <!-- <section class="search_area">
            <div class="search_inner">
                <input type="text" placeholder="Enter Your Search...">
                <i class="ti-close"></i>
            </div>
        </section> -->
        <!--================End Search Area =================-->



        <!--================Header Menu Area =================-->
            <?php require_once(dirname(__FILE__) . '/components/header.php'); ?>
        <!--================End Header Menu Area =================-->


        <!--================Slider Area =================-->
            <section id="home">
                <?php require_once(dirname(__FILE__) . '/components/sections/slider.php'); ?>
            </section>
        <!--================End Slider Area =================-->


        <!--================Creative Feature Area =================-->
            <section id="abouts">
                <?php require_once(dirname(__FILE__) . '/components/sections/features.php'); ?>
            </section>
        <!--================End Creative Feature Area =================-->


        <!--================About Area =================-->

        <!--================End About Area =================-->


        <!--================Our Service Area =================-->
            <section id="services">
                <?php require_once(dirname(__FILE__) . '/components/sections/services.php'); ?>
            </section>
        <!--================End Our Service Area =================-->


        <!--================Testimonials Area =================-->
        
        <!--================End Testimonials Area =================-->


        <!--================Project Area =================-->
            <section id="product"><
                <?php require_once(dirname(__FILE__) . '/components/sections/products.php'); ?>
            </section>
        <!--================End Project Area =================-->


        <!--================Products Area =================-->
            <section id="contact">
                <?php require_once(dirname(__FILE__) . '/components/sections/contact.php'); ?>
            </section>
        <!--================End Products Area =================-->


        <!--================Footer Area =================-->
            <?php require_once(dirname(__FILE__) . '/components/footer.php'); ?>
        <!--================End Footer Area =================-->




        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="assets/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="assets/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="assets/vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="assets/vendors/counterup/jquery.counterup.min.js"></script>
        <script src="assets/vendors/counterup/apear.js"></script>
        <script src="assets/vendors/counterup/countto.js"></script>
        <script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="assets/vendors/parallaxer/jquery.parallax-1.1.3.js"></script>
        <!--Tweets-->
        <!-- <script src="assets/vendors/tweet/tweetie.min.js"></script> -->
        <!-- <script src="assets/vendors/tweet/script.js"></script> -->

        <script src="assets/js/theme.js"></script>
    </body>
</html>
