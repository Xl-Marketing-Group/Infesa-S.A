<?php

class Carouseldata
{

    public function consultData()
    {
            $data = array(
                '0' => array(
                        'img'   => 'slider-1.jpg',
                        'id'    => '001',
                        'class' => 'active',
                        'title' => 'INFENSA',
                        'descr' => 'Industrias Fernandez - Puertas Placarol',
                        'btn'   => 'Catálogo  <i class="fa fa-download"></i>',
                        'route' => 'Serie Legacy.pdf',
                    ),
                '1' => array(
                        'img'   => 'slider-2.jpg',
                        'title' => 'Simple, Fast And Fun',
                        'id'    => '002',
                        'class' => '',
                        'descr' => 'The Fancy that recognize the talent and effort of the best web designers, develop-ers and agencies in the world.',
                        'btn'   => '',
                        'router'=> '',
                    ),
                '2' => array(
                        'img'   => 'slider-3.jpg',
                        'title' => 'Device Friendly',
                        'id'    => '003',
                        'class' => '',
                        'descr' => 'The Fancy that recognize the talent and effort of the best web designers, develop-ers and agencies in the world.',
                        'btn'   => '',
                        'router'=> '',
                    ),
                // '3' => array(
                //         'img'   => 'slider-4.jpg',
                //         'id'    => '004',
                //         'class' => '',
                //         'title' => 'We Create Experiences',
                //         'descr' => 'The Fancy that recognize the talent and effort of the best web designers, develop-ers and agencies in the world.',
                //     ),
                // '4' => array(
                //         'img'   => 'slider-5.jpg',
                //         'title' => 'Simple, Fast And Fun',
                //         'id'    => '005',
                //         'class' => '',
                //         'descr' => 'The Fancy that recognize the talent and effort of the best web designers, develop-ers and agencies in the world.',
                //     ),
            );

        return $data;
    }
}
