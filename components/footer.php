<footer class="footer_area">
    <div class="footer_widgets_area">
        <div class="container">
            <div class="f_widgets_inner row">
                <!-- <div class="col-lg-4 col-md-6">
                    <aside class="f_widget subscribe_widget">
                        <div class="f_w_title">
                            <h3>Our Newsletter</h3>
                        </div>
                        <p>Subscribe to our mailing list to get the updates to your email inbox.</p>
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="E-mail" aria-label="E-mail">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary submit_btn" type="button">Subscribe</button>
                            </span>
                        </div>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-4 col-md-6">
                    <aside class="f_widget contact_widget">
                        <div class="f_w_title">
                            <h3>Contact Us</h3>
                        </div>
                        <a href="#">1 (800) 686-6688</a>
                        <a href="#">info.deercreative@gmail.com</a>
                        <p>40 Baria Sreet 133/2 <br />NewYork City, US</p>
                        <h6>Open hours: 8.00-18.00 Mon-Fri</h6>
                    </aside>
                </div>
                <div class="col-lg-4 col-md-6">
                    <aside class="f_widget contact_widget">
                        <div class="f_w_title">
                            <h3>Contact Us</h3>
                        </div>
                        <a href="#">1 (800) 686-6688</a>
                        <a href="#">info.deercreative@gmail.com</a>
                        <p>40 Baria Sreet 133/2 <br />NewYork City, US</p>
                        <h6>Open hours: 8.00-18.00 Mon-Fri</h6>
                    </aside>
                </div> -->

                <div class="col-md-12 text-center">
                    <img src="assets/img/logo-footer.png" width="100">
                    <h6>INFENSA Industrias Fernandez - Puertas Placarol</h6>
                    <aside class="f_widget subscribe_widget  d-flex justify-content-center">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </aside>
                </div>

            </div>
        </div>
    </div>
    <div class="copy_right_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-5 d-flex justify-content-center">
                    <div class="float-md-left">
                        <h5>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved</h5>
                    </div>
                </div>
            </div>
            <!-- <div class="float-md-right">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Disclaimer</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Privacy</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Advertisement</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact us</a>
                    </li>
                </ul>
            </div> -->
        </div>
    </div>
</footer>