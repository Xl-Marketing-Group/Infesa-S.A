<?php
  require_once('./controllers/carouselController.php');
?>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <!-- <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol> -->
  <div class="carousel-inner" role="listbox">

    <?php
    $portfolio  = new Carousel();
    $result2    = $portfolio->consultCarousel();
    foreach ($result2['data'] as $pf) {
        if ($pf["btn"]) {
            echo '
            <div class="carousel-item '.$pf["class"].'">
              <img src="assets/img/home-slider/'.$pf["img"].'" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h3>'.$pf["title"].'</h3>
                    <p>'.$pf["descr"].'</p>
                    <a class="tp_btn" href="assets/catalogue/'.$pf["route"].'" download>'.$pf["btn"].'</a>
                </div>
            </div>
          ';
        } else {
            echo '
            <div class="carousel-item '.$pf["class"].'">
              <img src="assets/img/home-slider/'.$pf["img"].'" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h3>'.$pf["title"].'</h3>
                    <p>'.$pf["descr"].'</p>
                </div>
            </div>
          ';
        }
    }
    ?> 

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> -->
    <i class="ti-angle-left"></i>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span> -->
    <i class="ti-angle-right"></i>
    <span class="sr-only">Next</span>
  </a>
</div>
