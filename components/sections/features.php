<section class="creative_feature_area">
    <div class="container">
        <div class="c_feature_box">
            <div class="row">
                <div class="col-lg-4">
                    <div class="c_box_item">
                        <h4><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Confiabilidad</h4>
                        <!-- <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p> -->
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="c_box_item">
                        <h4><i class="fa fa-clock-o" aria-hidden="true"></i> Experiencia</h4>
                        <!-- <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p> -->
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="c_box_item">
                        <h4><i class="fa fa-diamond" aria-hidden="true"></i> Calidad</h4>
                        <!-- <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="digital_feature">
            <div class="row">
                <div class="col-lg-6">
                    <div class="d_feature_text">
                        <div class="main_title">
                            <h2>Somos especialistas en la fabricación de:</h2>
                        </div>
                        <p>Puertas, marcos, muebles, closets, molduras y derivados de madera para todo tipo de proyectos y en cualquier rango.</p>
                        <p>Creadores de ideas propuestas por nuestros clientes y en constante desarrollo de productos nuevos, todos pensados y probados para ofrecer calidad.</p>
                        <p>Entregados con nuestros compromisos para darles el apoyo necesario a nuestros clientes para culminar exitosamente sus proyectos.</p>
                        <!-- <a class="read_btn" href="#">Read more</a> -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="d_feature_img">
                        <img src="assets/img/feature-right.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
