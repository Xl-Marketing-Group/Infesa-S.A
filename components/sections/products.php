<?php
  require_once('./controllers/productsController.php');
?>

<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <h2>Productos</h2>
            <p>Desarrollamos productos de calidad y con máxima experiencia.</p>
        </div>
        <div class="l_news_inner">
            <div class="row">
                <?php
                $portfolio  = new Products();
                $result2    = $portfolio->consultProducts();
                $n = 1;
                foreach ($result2['data'] as $pf) {
                    echo '
                    <div class="col-lg-4 col-md-6 products-items">
                        <div class="l_news_item">
                            <div class="l_news_img"><a href="#"><img class="img-fluid" src="assets/img/products/'.$pf["img"].'" alt=""></a></div>
                            <div class="l_news_content">
                                <a href="#"><h4>'.$pf["title"].'</h4></a>
                                <p>'.$pf["descr"].'</p>
                                <!--<a class="more_btn" href="#">Learn More</a>-->
                            </div>
                        </div>
                    </div>
                  ';
                }
                ?> 
            </div>
        </div>
    </div>
</section>
