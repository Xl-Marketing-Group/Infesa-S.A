<section class="contact_section">
    <div class="container">
        <div class="b_center_title">
            <h2>Contáctanos</h2>
            <p>Estamos para servirte</p>
        </div>
        <div class="l_news_inner">
            <div class="row icon-contacts justify-content-center f-column">
                <div class="col-md-3 col-icons d-flex flex-column">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <small>Dirección</small>
                    <p>Calle 2da Llano Bonito, Juan Díaz</p>
                </div>
                <div class="col-md-3 col-icons d-flex flex-column">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <small>Teléfonos</small>
                    <p>+507 233-0505 / +507 233-2365</p>
                </div>
                <div class="col-md-3 col-icons d-flex flex-column">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <small>Correo electrónico</small>
                    <p>info@ginfesa.com</p>
                </div>
            </div>

           <!--  <form>
                <div class="row">
                    <div class="col-contact col-lg-6">
                        <div class="input-group">
                            <input type="text" class="form-control" required="required" placeholder="Razón social" aria-label="Razón social">
                        </div>
                    </div>
                    <div class="col-contact col-lg-6">
                        <div class="input-group">
                            <input type="email" class="form-control" required="required" placeholder="Correo electrónico" aria-label="Correo electrónico">
                        </div>
                    </div>
                    <div class="col-contact col-lg-12">
                        <div class="input-group">
                            <input type="text" class="form-control" required="required" placeholder="Asunto" aria-label="Asunto">
                        </div>
                    </div>
                    <div class="col-contact col-lg-12">
                        <div class="input-group">
                            <textarea class="form-control" required="required" placeholder="Mensaje" aria-label="Mensaje"></textarea>
                        </div>
                    </div>
                    <div class="col-contact col-lg-12 text-center">
                        <button class="btn btn-primary">Enviar</button>
                    </div>
                </div>
            </form> -->

            <div class="google-maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3940.3002193014877!2d-79.46284043533547!3d9.036354493515763!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1scalle+2da+Calle+2da+Llano+Bonito%2C+Juan+D%C3%ADaz+panama!5e0!3m2!1ses-419!2sve!4v1535319702425" width="950" height="300 frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

        </div>
    </div>
</section>
