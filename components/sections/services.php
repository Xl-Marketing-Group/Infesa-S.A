<section class="service_area">
    <div class="container">
        <div class="center_title">
            <h2>Servicios</h2>
            <p>Contamos con variedad de estilos y diseños dedicados a tus gustos y necesidades.</p>
        </div>
        <div class="row service_item_inner justify-content-center">
            <div class="col-lg-3">
                <div class="service_item">
                    <!-- <i class="ti-ruler-pencil"></i> -->
                    <div class="img-servs">
                        <img src="assets/img/services/9.jpg">
                    </div>
                    <h4>Serie Prime</h4>
                    <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                </div>
            </div>
            <div class="col-lg-3">
                <div class="service_item">
                    <!-- <i class="ti-desktop"></i> -->
                    <div class="img-servs">
                        <img src="assets/img/services/12.jpg">
                    </div>
                    <h4>Serie Angels</h4>
                    <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                </div>
            </div>
            <div class="col-lg-3">
                <div class="service_item">
                    <!-- <i class="ti-announcement"></i> -->
                    <div class="img-servs">
                        <img src="assets/img/services/15.jpg">
                    </div>
                    <h4>Serie Legacy</h4>
                    <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                </div>
            </div>
            <div class="col-lg-3">
                <div class="service_item">
                    <!-- <i class="ti-ruler-pencil"></i> -->
                    <div class="img-servs">
                        <img src="assets/img/services/15.jpg">
                    </div>
                    <h4>Serie Contraenchapada</h4>
                    <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                </div>
            </div>
            <div class="col-lg-3 mt-5">
                <div class="service_item">
                    <!-- <i class="ti-desktop"></i> -->
                    <div class="img-servs">
                        <img src="assets/img/services/16.jpg">
                    </div>
                    <h4>Serie Prestige</h4>
                    <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                </div>
            </div>
            <div class="col-lg-3 mt-5">
                <div class="service_item">
                    <!-- <i class="ti-announcement"></i> -->
                    <div class="img-servs">
                        <img src="assets/img/services/19.jpg">
                    </div>
                    <h4>Serie Luxury</h4>
                    <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                </div>
            </div>
            <div class="col-lg-3 mt-5">
                <div class="service_item">
                    <!-- <i class="ti-announcement"></i> -->
                    <div class="img-servs">
                        <img src="assets/img/services/25.jpg">
                    </div>
                    <h4>Serie Elegance</h4>
                    <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                </div>
            </div>
        </div>
    </div>
</section>